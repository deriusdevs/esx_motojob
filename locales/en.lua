Locales['en'] = {
  ['moto_club']                     = 'Мотоклуб',
  ['moto_club_car_sale']            = 'Продажа транспорта',
  ['moto_club_menu_title']          = 'Меню мотоклуба',
  ['moto_club_control']             = 'Управление мотоклубом',
  ['moto_car']                      = 'Служебный фургон',
  ['deposit_stock']                 = 'Положить на склад',
  ['withdraw_stock']                = 'Взять со склада',
  ['open_actions']                  = 'Нажмите ~INPUT_CONTEXT~ чтобы открыть меню',
  ['biker_menu_title']              = 'Меню байкера',
  ['hijack']                        = 'Вскрыть транспорт',
  ['hijack_failed']                 = '~r~Не удалось вскрыть транспорт',
  ['vehicle_unlocked']              =  '~g~Транспорт открыт',
  ['inventory']                     = 'Инвентарь',
  ['quantity']                      = 'Введите количество:',
  ['biker_stock']                   = 'Склад',
  ['you_removed']                   = 'Вы взяли со склада x',
  ['you_added']                     = 'Вы положили на склад x',
  ['invalid_quantity']              = 'Недопустимое количество',
  ['press_to_delete_veh']           = 'Нажмите ~INPUT_CONTEXT~ чтобы припарковать авто',

  -- Продажа авто
  ['press_to_sell_vehicle']         = 'Нажмите ~INPUT_CONTEXT~ чтобы продать транспортное средство',
  ['vehicle_sell_started']          = '~y~Выполняется продажа. Ожидайте.',
  ['vehicle_sell_error']            = '~r~Вы покинули место продажи.',
  ['vehicle_sell_success']          = '~g~Транспортное средство продано за ',

  -- Продажа травы
  ['blip_title_weed_sell']          = 'Продажа сорняков',
  ['press_to_sell_weed']            = 'Нажмите ~INPUT_CONTEXT~ чтобы продать сорняки',
  ['no_weed_to_sell']               = 'Нет сорняков...',
  ['start_sell_weed']               = 'Продаем сорняки...',

  -- Сбор винограда
  ['blip_title_grapes_coll']        = 'Сбор винограда',
  ['press_to_grapes_collection']    = 'Нажмите ~INPUT_CONTEXT~ чтобы собирать виноград',
  ['gropes_max_qnt']                = '~r~Вы собрали максимальное кол-во винограда',
  ['start_grapes_collect']          = 'Сбор винограда...',

  -- Сбор хмеля
  ['blip_title_hop_coll']           = 'Сбор хмеля',
  ['press_to_hop_collection']       = 'Нажмите ~INPUT_CONTEXT~ чтобы собирать хмель',
  ['hop_max_qnt']                   = '~r~Вы собрали максимальное кол-во хмеля',
  ['start_hop_collect']             = 'Сбор хмеля...',

  -- Магазин
  ['ingredients_shop_title']        = 'Магазин ингредиентов',
  ['press_to_open_shop']            = 'Нажмите ~INPUT_CONTEXT~ чтобы открыть меню',
  ['shop_sugar']                    = '🍚 Сахар',
  ['shop_no_money']                 = 'Нет денег',

  -- Крафт алкоголя
  ['press_to_craft_alcohol']        = 'Нажмите ~INPUT_CONTEXT~ чтобы начать разлив алкоголя',
  ['alcohol_craft_title']           = 'Разлив алкоголя',
  ['alcohol_craft_vine']            = '🍷 Крафтовое вино',
  ['alcohol_craft_beer']            = '🍺 Крафтовое пиво',
  ['alcohol_craft_few_ingredients'] = '~r~Мало ингредиентов',
  ['alcohol_craft_pour_wine']       = 'Разливаем вино...',
  ['alcohol_craft_pour_beer']       = 'Разливаем пиво...',
}