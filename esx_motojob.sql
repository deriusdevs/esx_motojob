USE `essentialmode`;

INSERT INTO `addon_account` (name, label, shared) VALUES
  ('society_moto','Мотоклуб',1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
  ('society_moto','Мотоклуб',1)
;

INSERT INTO `jobs` (name, label) VALUES
  ('moto','Мотоклуб')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  ('moto',0,'recrue','Новичок',150,'{}','{}'),
  ('moto',1,'novice','Кандидат',250,'{}','{}'),
  ('moto',2,'experimente','Помощник',450,'{}','{}'),
  ('moto',3,'chief','Вице-президент',600,'{}','{}'),
  ('moto',4,'boss','Президент',800,'{}','{}')
;


INSERT INTO `items` (`name`, `label`, `limit`) VALUES
  ('grapes','🍇 Виноград', 25),
	('hop','🌿 Хмель', 25),
  ('sugar','🍚 Сахар', 25),
  ('home_beer','🍺 Крафтовое пиво', 25),
	('home_vine','🍷 Крафтовое вино', 25)
; 