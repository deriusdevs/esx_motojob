local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData              = {}
local GUI                     = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local Blips                   = {}

ESX                           = nil
GUI.Time                      = 0

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
  CreateMotoBlips()
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
  CreateMotoBlips()
end)

AddEventHandler('esx_motojob:hasEnteredMarker', function(zone)

  ESX.UI.Menu.CloseAll()

  -- Меню
  if zone == 'MotoClubActions' then
    CurrentAction     = 'moto_actions_menu'
    CurrentActionMsg  = _U('open_actions')
    CurrentActionData = {}
  end

  -- Продажа авто
  if zone == 'CarSale' then

    local playerPed = GetPlayerPed(-1)

    if IsPedInAnyVehicle(playerPed,  false) then

      local vehicle = GetVehiclePedIsIn(playerPed,  false)

      CurrentAction     = 'sell_vehicle'
      CurrentActionMsg  = _U('press_to_sell_vehicle')
      CurrentActionData = {vehicle = vehicle}

    end
  end

  -- Продажа травы
  if zone == 'SellWeed' then
    CurrentAction     = 'sell_weed'
    CurrentActionMsg  = _U('press_to_sell_weed')
    CurrentActionData = {}
  end

  -- Сбор винограда
  if zone == 'GrapesCollection' then
    CurrentAction     = 'grapes_collection'
    CurrentActionMsg  = _U('press_to_grapes_collection')
    CurrentActionData = {}
  end

  -- Сбор хмеля
  if zone == 'HopCollection' then
    CurrentAction     = 'hop_collection'
    CurrentActionMsg  = _U('press_to_hop_collection')
    CurrentActionData = {}
  end

  -- Магазин ингредиентов
  if zone == 'IngredientsShop' then
    CurrentAction     = 'ingredients_shop'
    CurrentActionMsg  = _U('press_to_open_shop')
    CurrentActionData = {}
  end

  -- Крафт алкоголя
  if zone == 'AlcoholCraft' then
    CurrentAction     = 'alcohol_craft'
    CurrentActionMsg  = _U('press_to_craft_alcohol')
    CurrentActionData = {}
  end

  -- Удаление авто
  if zone == 'CarDeleter' then
    local playerPed = GetPlayerPed(-1)

    if IsPedInAnyVehicle(playerPed,  false) then

      local vehicle = GetVehiclePedIsIn(playerPed,  false)

      CurrentAction     = 'car_deleter'
      CurrentActionMsg  = _U('press_to_delete_veh')
      CurrentActionData = {vehicle = vehicle}

    end
  end
end)

AddEventHandler('esx_motojob:hasExitedMarker', function(zone)
  CurrentAction = nil
  ESX.UI.Menu.CloseAll()

  -- Окончание продажи травы
  TriggerServerEvent('esx_motojob:stopSellWeed')

  -- Окончание сбора винограда
  TriggerServerEvent('esx_motojob:stopGrapesCollect')

  -- Окончание сбора хмеля
  TriggerServerEvent('esx_motojob:stopHopCollect')

  -- Окончание крафта пива
  TriggerServerEvent('esx_motojob:stopCraftBeer')

  -- Окончание крафта вина
  TriggerServerEvent('esx_motojob:stopCraftVine')
end)

-- Показ маркеров
Citizen.CreateThread(function()
  while true do
    Wait(0)
    if PlayerData.job ~= nil and PlayerData.job.name == 'moto' then

      local coords = GetEntityCoords(GetPlayerPed(-1))

      for k,v in pairs(Config.Zones) do
        if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
        end
      end
    end
  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    Wait(0)
    if PlayerData.job ~= nil and PlayerData.job.name == 'moto' then
      local coords      = GetEntityCoords(GetPlayerPed(-1))
      local isInMarker  = false
      local currentZone = nil
      for k,v in pairs(Config.Zones) do
        if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
          isInMarker  = true
          currentZone = k
        end
      end
      if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
        HasAlreadyEnteredMarker = true
        LastZone                = currentZone
        TriggerEvent('esx_motojob:hasEnteredMarker', currentZone)
      end
      if not isInMarker and HasAlreadyEnteredMarker then
        HasAlreadyEnteredMarker = false
        TriggerEvent('esx_motojob:hasExitedMarker', LastZone)
      end
    end
  end
end)

-- Key Controls
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)

        if CurrentAction ~= nil then

          SetTextComponentFormat('STRING')
          AddTextComponentString(CurrentActionMsg)
          DisplayHelpTextFromStringLabel(0, 0, 1, -1)

          if IsControlJustReleased(0, 38) and PlayerData.job ~= nil and PlayerData.job.name == 'moto' then

            -- Меню
            if CurrentAction == 'moto_actions_menu' then
                OpenMotoClubActionsMenu()
            end

            -- Продажа транспорта
            if CurrentAction == 'sell_vehicle' then

              --TriggerServerEvent('esx_service:disableService', 'moto')
              ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
              TriggerServerEvent('esx_motojob:startSellCar')
            end

            -- Трава
            if CurrentAction == 'sell_weed' then
              TriggerServerEvent('esx_motojob:startSellWeed')
            end

            -- Виноград
            if CurrentAction == 'grapes_collection' then
              TriggerServerEvent('esx_motojob:startGrapesCollect')
            end

            -- Хмель
            if CurrentAction == 'hop_collection' then
              TriggerServerEvent('esx_motojob:startHopCollect')
            end

            -- Магазин ингредиентов
            if CurrentAction == 'ingredients_shop' then
                OpenIngredientsShopActionsMenu()
            end

            -- Крафт алкоголя
            if CurrentAction == 'alcohol_craft' then
                OpenAlcoholCraftActionsMenu()
            end

            if CurrentAction == 'car_deleter' then
              ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
            end

            CurrentAction = nil
          end
        end

        -- Меню байкера
        if IsControlJustReleased(0, Keys['U']) and PlayerData.job ~= nil and PlayerData.job.name == 'moto' then
            OpenMobileBikerActionsMenu()
        end

       

    end
end)

-----------------------------
-- Moto Club Menu          --
-----------------------------
function OpenMotoClubActionsMenu()

  local elements = {
    {label = _U('moto_car'), value = 'spawn_vehicle'},
    {label = _U('deposit_stock'), value = 'put_stock'},
    {label = _U('withdraw_stock'), value = 'get_stock'}
  }

  -- Пункт для босса
  if Config.EnablePlayerManagement and PlayerData.job ~= nil and PlayerData.job.grade_name == 'boss' then
    table.insert(elements, {label = _U('moto_club_control'), value = 'boss_actions'})
  end

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'moto_actions',
    {
      title    = _U('moto_club_menu_title'),
      elements = elements
    },
    function(data, menu)

      if data.current.value == 'spawn_vehicle' then
        ESX.Game.SpawnVehicle('speedo', Config.CarSpawner.Pos, 240.76, function(vehicle)
          local playerPed = GetPlayerPed(-1)
          TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
        end)
      end
      -- Положить на склад
      if data.current.value == 'put_stock' then
        OpenPutStocksMenu()
      end

      -- Взять со склада
      if data.current.value == 'get_stock' then
        OpenGetStocksMenu()
      end

      -- Меню босса
      if data.current.value == 'boss_actions' then
        TriggerEvent('esx_society:openBossMenu', 'moto', function(data, menu)
          menu.close()
        end)
      end

    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'moto_actions_menu'
      CurrentActionMsg  = _U('open_actions')
      CurrentActionData = {}
    end
  )
end

-----------------------------
-- Ящик                    --
-----------------------------
function OpenGetStocksMenu()

  ESX.TriggerServerCallback('esx_motojob:getStockItems', function(items)

    print(json.encode(items))

    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('biker_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('esx_motojob:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

-----------------------------
-- Ящик                    --
-----------------------------
function OpenPutStocksMenu()

ESX.TriggerServerCallback('esx_motojob:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('esx_motojob:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

-----------------------------
-- Меню байкера            --
-----------------------------
function OpenMobileBikerActionsMenu()

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'mobile_biker_actions',
    {
      title    = _U('biker_menu_title'),
      elements = {{label = _U('hijack'), value = 'hijack_vehicle'}}
    },
    function(data, menu)

      -- Вскрытие транспорта
      if data.current.value == 'hijack_vehicle' then

        local playerPed = GetPlayerPed(-1)
        local coords    = GetEntityCoords(playerPed)

        if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 5.0) then

          local vehicle = nil
          local prob = math.random(100)

          if IsPedInAnyVehicle(playerPed, false) then
            vehicle = GetVehiclePedIsIn(playerPed, false)
          else
            vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)
          end

          if DoesEntityExist(vehicle) then
            TaskStartScenarioInPlace(playerPed, "WORLD_HUMAN_WELDING", 0, true)
            SetVehicleAlarm(vehicle, true)
            StartVehicleAlarm(vehicle)

            Citizen.CreateThread(function()
              Citizen.Wait(10000)
              if prob <= 50 then
                SetVehicleDoorsLocked(vehicle, 1)
                SetVehicleDoorsLockedForAllPlayers(vehicle, false)
                ClearPedTasksImmediately(playerPed)
                ESX.ShowNotification(_U('vehicle_unlocked'))
              else
                ESX.ShowNotification(_U('hijack_failed'))
                ClearPedTasksImmediately(playerPed)
              end
              menu.close()
            end)

          end

        end
      end

    end,
  function(data, menu)
    menu.close()
  end
  )
end

-----------------------------
-- Магазин ингредиентов    --
-----------------------------
function OpenIngredientsShopActionsMenu()

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'ingredients_shop_actions',
    {
      title    = _U('ingredients_shop_title'),
      elements = {
        {label = _U('shop_sugar')..' <span style="color:green">$10</span>', value = 'buy_sugar', cost = Config.SugarCost}
      }
    },
    function(data, menu)

      if data.current.value == 'buy_sugar' then

        local playerPed = GetPlayerPed(-1)
        local coords    = GetEntityCoords(playerPed)

        TriggerServerEvent('esx_motojob:buySugar','sugar', data.current.cost)
      end

    end,
  function(data, menu)
    menu.close()
  end
  )
end

-----------------------------
-- Крафт алкоголя          --
-----------------------------
function OpenAlcoholCraftActionsMenu()

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'water_shop_actions',
    {
      title    = _U('alcohol_craft_title'),
      elements = {
        {label = _U('alcohol_craft_vine'), value = 'home_vine'},
        {label = _U('alcohol_craft_beer'), value = 'home_beer'},
      }
    },
    function(data, menu)

      if data.current.value == 'home_vine' then

        local playerPed = GetPlayerPed(-1)
        local coords    = GetEntityCoords(playerPed)

        TriggerServerEvent('esx_motojob:startCraftVine')
        menu.close()
      end

      if data.current.value == 'home_beer' then

        local playerPed = GetPlayerPed(-1)
        local coords    = GetEntityCoords(playerPed)

        ESX.UI.Menu.CloseAll()
        TriggerServerEvent('esx_motojob:startCraftBeer')
      end

    end,
  function(data, menu)
    menu.close()
  end
  )
end

-----------------------------
--          Blips          --
-----------------------------
-- База
Citizen.CreateThread(function()
  local blip = AddBlipForCoord(Config.Zones.MotoClubActions.Pos.x, Config.Zones.MotoClubActions.Pos.y, Config.Zones.MotoClubActions.Pos.z)
  SetBlipSprite (blip, 226)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 1.3)
  SetBlipColour (blip, 76)
  SetBlipAsShortRange(blip, true)
  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString(_U('moto_club'))
  EndTextCommandSetBlipName(blip)
end)

function CreateMotoBlips()

  if PlayerData.job ~= nil and PlayerData.job.name == 'moto' then

    -- Косяк
    local blip = AddBlipForCoord(Config.Zones.SellWeed.Pos.x, Config.Zones.SellWeed.Pos.y, Config.Zones.SellWeed.Pos.z)
    SetBlipSprite(blip, 496)
    SetBlipColour(blip, 69)
    SetBlipDisplay(blip, 4)
    SetBlipScale(blip, 1.0)
    SetBlipAsShortRange(blip, true)
    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(_U('blip_title_weed_sell'))
    EndTextCommandSetBlipName(blip)

    -- Хмель
    blip = AddBlipForCoord(Config.Zones.HopCollection.Pos.x, Config.Zones.HopCollection.Pos.y, Config.Zones.HopCollection.Pos.z)
    SetBlipSprite (blip, 85)
    SetBlipDisplay(blip, 4)
    SetBlipScale  (blip, 1.2)
    SetBlipColour (blip, 83)
    SetBlipAsShortRange(blip, true)
    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(_U('blip_title_hop_coll'))
    EndTextCommandSetBlipName(blip)

    --Виноград
    blip = AddBlipForCoord(Config.Zones.GrapesCollection.Pos.x, Config.Zones.GrapesCollection.Pos.y, Config.Zones.GrapesCollection.Pos.z)
    SetBlipSprite (blip, 85)
    SetBlipDisplay(blip, 4)
    SetBlipScale  (blip, 1.2)
    SetBlipColour (blip, 83)
    SetBlipAsShortRange(blip, true)
    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(_U('blip_title_grapes_coll'))
    EndTextCommandSetBlipName(blip)

    -- Продажа авто
    blip = AddBlipForCoord(Config.Zones.CarSale.Pos.x, Config.Zones.CarSale.Pos.y, Config.Zones.CarSale.Pos.z)
    SetBlipSprite (blip, 467)
    SetBlipDisplay(blip, 4)
    SetBlipScale  (blip, 1.3)
    SetBlipColour (blip, 77)
    SetBlipAsShortRange(blip, true)
    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(_U('moto_club_car_sale'))
    EndTextCommandSetBlipName(blip)

  end

end