ESX = nil
local PlayersSellingWeed = {}
local PlayersGrapesCol   = {}
local PlayersHopCol      = {}
local PlayersCraftVine   = {}
local PlayersCraftBeer   = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

--if Config.MaxInService ~= -1 then
  --TriggerEvent('esx_service:activateService', 'moto', Config.MaxInService)
--end

TriggerEvent('esx_society:registerSociety', 'moto', 'Moto', 'society_moto', 'society_moto', 'society_moto', {type = 'private'})

-----------------------------
-- Moto Club Inventory Get --
-----------------------------
RegisterServerEvent('esx_motojob:getStockItem')
AddEventHandler('esx_motojob:getStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_moto', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then

      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)

      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)

    else

      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))

    end

  end)

end)

ESX.RegisterServerCallback('esx_motojob:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_moto', function(inventory)

    cb(inventory.items)

  end)

end)

-----------------------------
-- Moto Club Inventory Put --
-----------------------------
RegisterServerEvent('esx_motojob:putStockItems')
AddEventHandler('esx_motojob:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_moto', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if item.count >= 0 and count <= playerItemCount then

      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)

      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. item.label)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

  end)

end)

ESX.RegisterServerCallback('esx_motojob:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({items = items})

end)

-----------------------------
--       Продажа тс        --
-----------------------------
RegisterServerEvent('esx_motojob:startSellCar')
AddEventHandler('esx_motojob:startSellCar', function(v)
  
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  local total   = math.random(Config.SellVehicleEarnMin, Config.SellVehicleEarnMax)

  TriggerEvent('esx_addonaccount:getSharedAccount', 'society_moto', function(account)
    account.addMoney(total)
    xPlayer.addMoney(total)
  end)

  TriggerClientEvent('esx:showNotification', source, _U('vehicle_sell_success') ..total..'$')

end)

-----------------------------
-- Виноград                --
-----------------------------
local function GrapesCollect(source)
  SetTimeout(5000, function()

    if PlayersGrapesCol[source] == true then

      local _source = source
      local xPlayer = ESX.GetPlayerFromId(_source)

      -- Кол-во винограда
      local grapesQuantity = xPlayer.getInventoryItem('grapes').count

      if grapesQuantity >= 25 then
        -- Уведомление о переборе
        TriggerClientEvent('esx:showNotification', source, _U('gropes_max_qnt'))
      else
        -- Добавляем
        xPlayer.addInventoryItem('grapes', 1)
               
        -- Повторяем
        GrapesCollect(source)
      end
    end

  end)
end

-- Начало сбора винограда
RegisterServerEvent('esx_motojob:startGrapesCollect')
AddEventHandler('esx_motojob:startGrapesCollect', function()

  local _source = source

  PlayersGrapesCol[_source] = true

  TriggerClientEvent('esx:showNotification', _source, _U('start_grapes_collect'))

  GrapesCollect(_source)

end)

-- Окончание сбора винограда
RegisterServerEvent('esx_motojob:stopGrapesCollect')
AddEventHandler('esx_motojob:stopGrapesCollect', function()

  local _source = source

  PlayersGrapesCol[_source] = false

end)

-----------------------------
-- Хмель                   --
-----------------------------
local function HopCollect(source)
  SetTimeout(5000, function()

    if PlayersHopCol[source] == true then

      local _source = source
      local xPlayer = ESX.GetPlayerFromId(_source)

      -- Кол-во винограда
      local hopQuantity = xPlayer.getInventoryItem('hop').count

      if hopQuantity >= 25 then
        -- Уведомление о переборе
        TriggerClientEvent('esx:showNotification', source, _U('hop_max_qnt'))
      else
        -- Добавляем
        xPlayer.addInventoryItem('hop', 1)
               
        -- Повторяем
        HopCollect(source)
      end
    end

  end)
end

-- Начало сбора хмеля
RegisterServerEvent('esx_motojob:startHopCollect')
AddEventHandler('esx_motojob:startHopCollect', function()

  local _source = source

  PlayersHopCol[_source] = true

  TriggerClientEvent('esx:showNotification', _source, _U('start_hop_collect'))

  HopCollect(_source)

end)

-- Окончание сбора хмеля
RegisterServerEvent('esx_motojob:stopHopCollect')
AddEventHandler('esx_motojob:stopHopCollect', function()

  local _source = source

  PlayersHopCol[_source] = false

end)

-----------------------------
-- Трава                   --
-----------------------------
local function SellWeed(source)

  SetTimeout(5000, function()

    if PlayersSellingWeed[source] == true then

      local _source = source
      local xPlayer = ESX.GetPlayerFromId(_source)

      -- Кол-во сорняков
      local weedQnt = xPlayer.getInventoryItem('weed').count

      if weedQnt == 0 then
        -- Уведомление о нехватке
        TriggerClientEvent('esx:showNotification', source, _U('no_weed_to_sell'))
      else
        -- Удаляем
        xPlayer.removeInventoryItem('weed', 1)

        -- Начисляем деньги
        xPlayer.addAccountMoney('black_money', Config.WeedCost)

        -- Показываем уведомление
        --TriggerClientEvent('esx:showNotification', source, _U('sold_one_coke'))
               
        -- Повторяем
        SellWeed(source)
      end
    end

  end)
end

-- Начало продажи травы
RegisterServerEvent('esx_motojob:startSellWeed')
AddEventHandler('esx_motojob:startSellWeed', function()

  local _source = source
  PlayersSellingWeed[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('start_sell_weed'))
  SellWeed(_source)

end)

-- Окончание продажи травы
RegisterServerEvent('esx_motojob:stopSellWeed')
AddEventHandler('esx_motojob:stopSellWeed', function()

  local _source = source

  PlayersSellingWeed[_source] = false

end)

-- Покупка сахара
RegisterServerEvent('esx_motojob:buySugar')
AddEventHandler('esx_motojob:buySugar', function(name, cost)

  local _source = source
  local xPlayer  = ESX.GetPlayerFromId(source)

  local sugarQnt = xPlayer.getInventoryItem('sugar').count
  local grapesQnt = xPlayer.getInventoryItem('grapes').count

  if xPlayer.get('money') >= cost then

    xPlayer.removeMoney(cost)
    xPlayer.addInventoryItem(name, 1)

  else
    TriggerClientEvent('esx:showNotification', _source, _U('shop_no_money'))
  end

end)


-----------------------------
-- Крафт вина              --
-----------------------------
local function CraftVine(source)

  SetTimeout(5000, function()

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    
    if PlayersCraftVine[source] == true then

      -- Кол-во сахара
      local sugarQnt = xPlayer.getInventoryItem('sugar').count
      
      -- Кол-во винограда
      local grapesQnt = xPlayer.getInventoryItem('grapes').count

      if sugarQnt >= 5 and grapesQnt >= 5 then
        -- Удаляем
        xPlayer.removeInventoryItem('sugar', 5)
        xPlayer.removeInventoryItem('grapes', 5)

        -- Добавляем вино
        xPlayer.addInventoryItem('home_vine', 1)
               
        -- Повторяем
        CraftVine(source)
      else
        TriggerClientEvent('esx:showNotification', source, _U('alcohol_craft_few_ingredients'))
      end
    end

  end)
end

-- Начало крафта вина
RegisterServerEvent('esx_motojob:startCraftVine')
AddEventHandler('esx_motojob:startCraftVine', function()

  local _source = source
  PlayersCraftVine[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('alcohol_craft_pour_wine'))
  CraftVine(_source)

end)

-- Окончание крафта вина
RegisterServerEvent('esx_motojob:stopCraftVine')
AddEventHandler('esx_motojob:stopCraftVine', function()

  local _source = source

  PlayersCraftVine[_source] = false

end)

-----------------------------
-- Крафт пива              --
-----------------------------
local function CraftBeer(source)

  SetTimeout(5000, function()

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    
    if PlayersCraftBeer[source] == true then

      -- Кол-во сахара
      local sugarQnt = xPlayer.getInventoryItem('sugar').count
      
      -- Кол-во хмеля
      local hopQnt = xPlayer.getInventoryItem('hop').count

      if sugarQnt >= 5 and hopQnt >= 5 then
        -- Удаляем
        xPlayer.removeInventoryItem('sugar', 5)
        xPlayer.removeInventoryItem('hop', 5)

        -- Добавляем пиво
        xPlayer.addInventoryItem('home_beer', 1)
               
        -- Повторяем
        CraftBeer(source)
      else
        TriggerClientEvent('esx:showNotification', source, _U('alcohol_craft_few_ingredients'))
      end
    end

  end)
end

-- Начало крафта пива
RegisterServerEvent('esx_motojob:startCraftBeer')
AddEventHandler('esx_motojob:startCraftBeer', function()

  local _source = source
  PlayersCraftBeer[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('alcohol_craft_pour_beer'))
  CraftBeer(_source)

end)

-- Окончание крафта пива
RegisterServerEvent('esx_motojob:stopCraftBeer')
AddEventHandler('esx_motojob:stopCraftBeer', function()

  local _source = source

  PlayersCraftBeer[_source] = false

end)