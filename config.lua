Config                          = {}
Config.DrawDistance             = 100.0
Config.EnablePlayerManagement   = true
Config.SellVehicleEarnMin       = 250  -- Минимальная цена за тс
Config.SellVehicleEarnMax       = 2500 -- Максимальная цена за тс
Config.Locale                   = 'ru'
Config.SugarCost                = 10 -- Цена сахара
Config.WeedCost                 = 100 -- Цена та косяк
--Config.EnableSocietyOwnedVehicles = false
--Config.MaxInService               = -1

Config.Zones = {

  MotoClubActions = {
    Pos   = { x = 953.71, y = -122.12, z = 73.10 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 1,
  },
  
  CarSale = {
    Pos   = { x = -372.56, y = -97.99, z = 44.45 },
    Size  = { x = 3.0, y = 3.0, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 1,
  },

  SellWeed = {
    Pos   = { x = 157.43, y = 3131.09, z = 42.58 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 0, g = 127, b = 14 },
    Type  = 1,
  },

  GrapesCollection = {
    Pos   = { x = -1766.18, y = 2342.48, z = 58.81 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 178, g = 0, b = 255 },
    Type  = 1,
  },

  HopCollection = {
    Pos   = { x = 336.13, y = 6672.7, z = 27.8 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 0, g = 127, b = 14 },
    Type  = 1,
  },

  IngredientsShop = {
    Pos   = { x = 946.8, y = -120.57, z = 73.35 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 0, g = 127, b = 14 },
    Type  = 1,
  },

  AlcoholCraft = {
    Pos   = { x = 951.47, y = -119.87, z = 73.35 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 0, g = 127, b = 14 },
    Type  = 1,
  },

  CarDeleter = {
    Pos   = { x = 986.11, y = -138.42, z = 72.09 },
    Size  = { x = 3.0, y = 3.0, z = 1.0 },
    Color = { r = 0, g = 127, b = 14 },
    Type  = 1,
  },
}

Config.CarSpawner = {
  Pos   = { x = 954.83, y = -133.62, z = 73.88 },
  Size  = { x = 1.5, y = 1.5, z = 1.0 },
  Color = { r = 0, g = 127, b = 14 },
  Type  = 1,
}